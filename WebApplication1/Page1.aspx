﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page1.aspx.cs" Inherits="WebApplication1.Page1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-group">
        <h3>Manage Pages</h3>
        <div class="form-group">
            <label class="control-label col-sm-2">Search:</label>
            <asp:TextBox runat="server" ID="pet_search" CssClass="form-control"></asp:TextBox>
            <asp:Button runat="server" Text="Search" CssClass="btn btn-primary" OnClick="SearchPage" />
        </div>
        <asp:SqlDataSource runat="server" ID="page_select"
            ConnectionString="<%$ ConnectionStrings:page_con %>"></asp:SqlDataSource>
        <asp:DataGrid ID="page_list"
            runat="server">
        </asp:DataGrid>
    </div>
    <div>
        <asp:Button runat="server" Text="Add New Page" CssClass="btn btn-primary" OnClick="AddPage" />
    </div>
</asp:Content>
