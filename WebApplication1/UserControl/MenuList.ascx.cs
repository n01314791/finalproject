﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1.UserControl
{
    public partial class MenuList : System.Web.UI.UserControl
    {
        private string query = "select * from pages";
        protected void Page_Load(object sender, EventArgs e)
        {
            menu_page.SelectCommand = query;
            DataView view = (DataView)menu_page.Select(DataSourceSelectArguments.Empty);

            string menu_text = "";
            foreach (DataRowView row in view)
            {          
                menu_text += "<li><a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a></li>";
            }
            menubox.InnerHtml =menu_text;
        }
    }
}