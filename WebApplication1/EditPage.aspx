﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="WebApplication1.EditPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form-group">
        <h3 runat="server" id="class_edit">Edit Page</h3>
    </div>
    <asp:SqlDataSource runat="server" ID="edit_page"
        ConnectionString="<%$ ConnectionStrings:page_con %>"></asp:SqlDataSource>
    <div class="form-group">
        <label class="control-label col-sm-2">Page Title:</label>
        <asp:TextBox runat="server" ID="Page_Title"  CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="Page_Title"
            ErrorMessage="Enter a page name">
        </asp:RequiredFieldValidator>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2">Page Content:</label>
        <asp:TextBox runat="server" ID="page_content" TextMode="MultiLine" Columns="50" Rows="10" CssClass="form-control"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_content"
            ErrorMessage="Enter a page content">
        </asp:RequiredFieldValidator>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <asp:Button runat="server" Text="Edit" OnClick="Edit_Click" CssClass="btn btn-primary" />
        </div>
    </div>
</asp:Content>
