﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class Page1 : System.Web.UI.Page
    {
        private string pageQuery = "select pageid as ID,pagetitle as 'Page Name' from pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            page_select.SelectCommand = pageQuery;
            page_list.DataSource = Pages_Manual_Bind(page_select);
            page_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                row["Page Name"] = "<a href=\"Page.aspx?pageid="
                    + row["ID"]
                    + "\">"
                    + row["Page Name"]
                    + "</a>";
            }
            mytbl.Columns.Remove("ID");
            myview = mytbl.DefaultView;
            return myview;
        }

        protected void AddPage(object sender, EventArgs e)
        {
            Response.Redirect("NewPage.aspx");
        }

        protected void SearchPage(object sender, EventArgs e)
        {
            string searchsql = pageQuery + " WHERE (1=1)";
            string searchvalue = pet_search.Text.ToString();
            if (searchvalue != "" || searchvalue != null)
            {
                string searchtitle = searchsql + "and pagetitle like '%" + searchvalue + "%'";
                page_select.SelectCommand = searchtitle;
                page_list.DataSource = Pages_Manual_Bind(page_select);
                page_list.DataBind();
            }
        }
    }
}