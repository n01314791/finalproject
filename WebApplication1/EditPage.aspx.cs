﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class EditPage : System.Web.UI.Page
    {
        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            DataRowView pagerow = getPageInfo(pageid);
            if (pagerow == null)
            {
                class_edit.InnerHtml = "No Page Found";
                return;
            }
            else
            {
                Page_Title.Text = pagerow["pagetitle"].ToString();
                page_content.Text = pagerow["pagecontent"].ToString();
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            string title = Page_Title.Text;
            string content = page_content.Text;

            string editquery = "update pages set pagetitle ='" + title + "'," +
               " pagecontent ='" + content + "'" +
               " where pageid=" + pageid;
            edit_page.UpdateCommand = editquery;
            edit_page.Update();
            Response.Redirect("Page1.aspx");
        }
        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            edit_page.SelectCommand = query;

            DataView pageview = (DataView)edit_page.Select(DataSourceSelectArguments.Empty);

            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0];
            return pagerowview;
        }
    }
}