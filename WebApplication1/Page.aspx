﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Page.aspx.cs" Inherits="WebApplication1.Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2 id="header_element" runat="server"></h2>
    <div id="pagecontent" runat="server" class="form-group">
    </div>
    <asp:SqlDataSource runat="server" ID="page_select"
        ConnectionString="<%$ ConnectionStrings:page_con %>"></asp:SqlDataSource>
    <div class="form-group">
    <a href="EditPage.aspx?Pageid=<%Response.Write(this.pageid); %>" class="btn btn-info">Edit Page </a>
        </div>
    <asp:SqlDataSource
        runat="server"
        ID="del_class"
        ConnectionString="<%$ ConnectionStrings:page_con %>"></asp:SqlDataSource>
    <div class="form-group">
        <asp:Button runat="server" ID="del_class_but"
            OnClick="DeletePage"
            CssClass="btn btn-primary"
            OnClientClick="if(!confirm('Are you sure?')) return false;"
            Text="Delete" />
    </div>
</asp:Content>
