﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class Page : System.Web.UI.Page
    {
        public string pageid {
            get { return Request.QueryString["pageid"]; }
        }

        private string pageQuery = "select pageid,pagetitle,pagecontent from pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == null || pageid == "") header_element.InnerHtml = "No Page Found";
            else
            {
                pageQuery += " WHERE pageid=" + pageid;
                page_select.SelectCommand = pageQuery;
                DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
                DataRowView pagerowview = pageview[0];
                header_element.InnerHtml = pageview[0]["pagetitle"].ToString();
                pagecontent.InnerHtml = pageview[0]["pagecontent"].ToString();
            }
        }

        protected void DeletePage(object sender, EventArgs e)
        {
            string delquery = "DELETE from pages where pageid=" + pageid;
            del_class.DeleteCommand = delquery;
            del_class.Delete();
            Response.Redirect("Page1.aspx");
        }
    }
}